package com.example.mykolayevstihnieiev.achive.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.mykolayevstihnieiev.achive.R;
import com.example.mykolayevstihnieiev.achive.fragments.AssignedToMeFragment;
import com.example.mykolayevstihnieiev.achive.fragments.CalendarFragment;
import com.example.mykolayevstihnieiev.achive.fragments.MyTasksFragment;
import com.example.mykolayevstihnieiev.achive.fragments.ScoreboardFragment;
import com.example.mykolayevstihnieiev.achive.fragments.SearchCategoryFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private FragmentTransaction transaction;
    private Toolbar toolbar;
    private FloatingActionButton floatingActionButton;
    private BottomNavigationView bottomNavigationView;
    private MyTasksFragment homeFragment;
    private SearchCategoryFragment searchCategoryFragment;
    private ScoreboardFragment scoreboardFragment;
    private AssignedToMeFragment assignedToMeFragment;
    private CalendarFragment calendarFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        floatingActionButton = findViewById(R.id.fab);
        toolbar = findViewById(R.id.toolbar);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        createFragments();
        changeFragment("HeadWay", homeFragment);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.create_task) {
            addNote(item.getActionView());
        } else if (id == R.id.create_tack_group) {
            createNote(item.getActionView());
        } else if (id == R.id.invite) {
            Toast.makeText(this, "invite friend clicked", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.settings) {
            Toast.makeText(this, "settings clicked", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.contact_us) {
            Toast.makeText(this, "contact_us clicked", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.information) {
            Toast.makeText(this, "information clicked", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.footer_home_btn) {
            floatingActionButton.setVisibility(View.VISIBLE);
            changeFragment("HeadWay", homeFragment);
        } else if (id == R.id.footer_search_btn) {
            floatingActionButton.setVisibility(View.INVISIBLE);
            changeFragment("Chose CATEGORY", searchCategoryFragment);
        } else if (id == R.id.footer_scores_btn) {
            floatingActionButton.setVisibility(View.INVISIBLE);
            changeFragment("Scores", scoreboardFragment);
        } else if (id == R.id.footer_aim_btn) {
            floatingActionButton.setVisibility(View.INVISIBLE);
            changeFragment("Assigned to me", assignedToMeFragment);
        } else if (id == R.id.footer_calendar_btn) {
            floatingActionButton.setVisibility(View.INVISIBLE);
            changeFragment("Calendar", calendarFragment);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void shortItemClick(View v) {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    public void addNote(View v) {
        homeFragment.addNotes();
    }

    public void createNote(View v) {
        Intent i = new Intent(this, CreateTaskActivity.class);
        startActivity(i);
    }

    public void onFilterClicked(MenuItem item) {
        Toast.makeText(this, "onFilterClicked", Toast.LENGTH_SHORT).show();
    }

    private void changeFragment(String title, Fragment fragment) {
        toolbar.setTitle(title);
        transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void createFragments() {
        homeFragment = new MyTasksFragment();
        searchCategoryFragment = new SearchCategoryFragment();
        scoreboardFragment = new ScoreboardFragment();
        assignedToMeFragment = new AssignedToMeFragment();
        calendarFragment = new CalendarFragment();
    }
}
