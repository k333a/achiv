package com.example.mykolayevstihnieiev.achive.entities.Descriptors;

/**
 * Created by nikolai on 09.11.18.
 */

public class CategoryItemDescriptor {
    private String title;
    private int amaountOfTasks;
    private int categoryId;

    public CategoryItemDescriptor(String title, int amaountOfTasks, int categoryId) {
        this.title = title;
        this.amaountOfTasks = amaountOfTasks;
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public int getAmaountOfTasks() {
        return amaountOfTasks;
    }

    public int getCategoryId() {
        return categoryId;
    }
}

