package com.example.mykolayevstihnieiev.achive.views;

import android.content.Context;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mykolayevstihnieiev.achive.R;

public class CustomTextInputView extends RelativeLayout {
    private TextView textHint;
    private EditText content;
    private TextInputLayout editContentContainer;

    public CustomTextInputView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.cutom_text_input, this, true);

        textHint = findViewById(R.id.text_hint);
        content = findViewById(R.id.edit_content);
        editContentContainer = findViewById(R.id.edit_content_container);

        content.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Show white background behind floating label
                            textHint.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                } else {
                    // Required to show/hide white background behind floating label during focus change
                    if (content.getText().length() > 0)
                        textHint.setVisibility(View.VISIBLE);
                    else
                        textHint.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public TextView getTextHint() {
        return textHint;
    }

    public EditText getContent() {
        return content;
    }

    public TextInputLayout getEditContentContainer() {
        return editContentContainer;
    }
}
