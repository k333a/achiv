package com.example.mykolayevstihnieiev.achive;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mykolayevstihnieiev.achive.entities.Descriptors.CategoryItemDescriptor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by nikolai on 09.11.18.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private List<CategoryItemDescriptor> categoryItemsListDescriptor = new ArrayList<CategoryItemDescriptor>();

    public void setItems(Collection<CategoryItemDescriptor> items){
        categoryItemsListDescriptor.addAll(items);
        notifyDataSetChanged();
    }

    public void clearItems(){
        categoryItemsListDescriptor.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout_new_gen, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.bind(categoryItemsListDescriptor.get(position));
    }

    @Override
    public int getItemCount() {
        return categoryItemsListDescriptor.size();
    }

}
