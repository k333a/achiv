package com.example.mykolayevstihnieiev.achive.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mykolayevstihnieiev.achive.R;

public class ChooseItemView extends CardView {

    private CardView container;
    private ImageView image;
    private TextView text;

    public ChooseItemView(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.create_task_choose_item, this, true);

        initFields();
    }

    public ChooseItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.create_task_choose_item, this, true);
        initFields();
    }

    public void initFields() {
        container = findViewById(R.id.choose_item_container);
        image = findViewById(R.id.choose_item_image_container);
        text = findViewById(R.id.choose_item_text_container);
    }


    public CardView getContainer() {
        return container;
    }

    public ImageView getImage() {
        return image;
    }

    public TextView getText() {
        return text;
    }
}
