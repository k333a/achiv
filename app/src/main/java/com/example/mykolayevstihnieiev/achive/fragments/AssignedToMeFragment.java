package com.example.mykolayevstihnieiev.achive.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mykolayevstihnieiev.achive.R;

public class AssignedToMeFragment extends Fragment {
    private Context context;
    private ConstraintLayout content;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.not_implemented_fragment, container, false);

        context = inflater.getContext();
        return view;
    }
}
