package com.example.mykolayevstihnieiev.achive;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mykolayevstihnieiev.achive.entities.Descriptors.TaskItemDescriptor;

/**
 * Created by nikolai on 10.11.18.
 */

public class TaskViewHolder extends RecyclerView.ViewHolder {
    private ImageView categoryImage;
    private TextView title;

    public TaskViewHolder(View itemView) {
        super(itemView);
        categoryImage = itemView.findViewById(R.id.categoryImage);
        title = itemView.findViewById(R.id.title);
    }

    public void bind(TaskItemDescriptor item) {
        title.setText(item.getTitle());
        setCategoryImage(item.getCategotyImageId());
    }


    public void setCategoryImage(int categoryId) {
        switch (categoryId) {
            case 0:
                categoryImage.setImageResource(R.drawable.icon1);
                break;
            case 1:
                categoryImage.setImageResource(R.drawable.icon2);
                break;
            case 2:
                categoryImage.setImageResource(R.drawable.icon3);
                break;
            case 3:
                categoryImage.setImageResource(R.drawable.icon4);
                break;
            case 4:
                categoryImage.setImageResource(R.drawable.icon5);
                break;
            case 5:
                categoryImage.setImageResource(R.drawable.icon6);
                break;
            case 6:
                categoryImage.setImageResource(R.drawable.icon7);
                break;
            case 7:
                categoryImage.setImageResource(R.drawable.icon8);
                break;
            case 8:
                categoryImage.setImageResource(R.drawable.icon5);
                break;
        }
    }
}
