package com.example.mykolayevstihnieiev.achive.entities.Descriptors;

import com.example.mykolayevstihnieiev.achive.entities.Categories;

import java.util.Date;

public class TaskItemDescriptor {
    public double id;
    public int categotyImageId;
    public Categories categoty;
    public Date startDate;
    public Date completeDate;
    public String title;
    public String description;
    public String interestedBy;
    public String completedBy;

    public TaskItemDescriptor() {
    }

    public TaskItemDescriptor(int categotyImageId, String title) {
        this.categotyImageId = categotyImageId;
        this.title = title;
    }

    public TaskItemDescriptor(double id, int categotyImageId, Categories categoty, Date startDate, Date completeDate, String title, String description, String interestedBy, String completedBy) {
        this.id = id;
        this.categotyImageId = categotyImageId;
        this.categoty = categoty;
        this.startDate = startDate;
        this.completeDate = completeDate;
        this.title = title;
        this.description = description;
        this.interestedBy = interestedBy;
        this.completedBy = completedBy;
    }

    public double getId() {
        return id;
    }

    public int getCategotyImageId() {
        return categotyImageId;
    }

    public Categories getCategoty() {
        return categoty;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getInterestedBy() {
        return interestedBy;
    }

    public String getCompletedBy() {
        return completedBy;
    }
}