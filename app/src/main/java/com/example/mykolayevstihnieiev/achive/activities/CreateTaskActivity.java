package com.example.mykolayevstihnieiev.achive.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.mykolayevstihnieiev.achive.R;
import com.example.mykolayevstihnieiev.achive.views.ChooseItemView;

import java.util.Calendar;

public class CreateTaskActivity extends AppCompatActivity {
    Toolbar toolbar;
    ChooseItemView photos;
    ChooseItemView location;
    ChooseItemView access;
    TextView dates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_task);
        Window window = this.getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorAccentDark));

        toolbar = findViewById(R.id.create_task_toolbar);
        toolbar.setTitle(R.string.create_task_activity_title);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        photos = findViewById(R.id.photo_choose);
        location = findViewById(R.id.location_choose);
        access = findViewById(R.id.access_choose);
        dates = findViewById(R.id.task_dates);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setPhotosContent();
        setLocationContent();
        setAccessContent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_task_menu, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void setPhotosContent() {
        photos.initFields();
        photos.getContainer().setBackground(getResources().getDrawable(R.drawable.create_task_background_photos));
        photos.getText().setText("Photos");
        photos.getImage().setImageResource(R.drawable.common_choose_photos);
    }

    private void setLocationContent() {
        location.initFields();
        location.getContainer().setBackground(getResources().getDrawable(R.drawable.create_task_background_location));
        location.getText().setText("Location");
        location.getImage().setImageResource(R.drawable.common_location_add);
    }

    private void setAccessContent() {
        access.initFields();
        access.getContainer().setBackground(getResources().getDrawable(R.drawable.create_task_background_access));
        access.getText().setText("Access");
        access.getImage().setImageResource(R.drawable.common_group_icon);
    }

    public void getDialog(View v) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        dates.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);


        datePickerDialog.show();
    }
//        if (v == btnTimePicker) {
//
//        // Get Current Time
//        final Calendar c = Calendar.getInstance();
//        mHour = c.get(Calendar.HOUR_OF_DAY);
//        mMinute = c.get(Calendar.MINUTE);
//
//        // Launch Time Picker Dialog
//        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
//                new TimePickerDialog.OnTimeSetListener() {
//
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay,
//                                          int minute) {
//
//                        txtTime.setText(hourOfDay + ":" + minute);
//                    }
//                }, mHour, mMinute, false);
//        timePickerDialog.show();
//    }
//    }
}
