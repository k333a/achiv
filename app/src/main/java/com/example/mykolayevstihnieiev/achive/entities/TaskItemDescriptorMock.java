package com.example.mykolayevstihnieiev.achive.entities;

import com.example.mykolayevstihnieiev.achive.entities.Descriptors.TaskItemDescriptor;

import java.util.Date;

public class TaskItemDescriptorMock extends TaskItemDescriptor {
    public TaskItemDescriptorMock() {
        id = 1;
        categotyImageId = 1;
        categoty = Categories.SPORT;
        startDate = new Date();
        completeDate = new Date();
        title = "Visit Kiev";
        description = "Kyiv is the capital of Ukraine";
        interestedBy = "1.7k are interested";
        completedBy = "2.3k complete";
    }

    public void setId(double id) {
        this.id = id;
    }
}
