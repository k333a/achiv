package com.example.mykolayevstihnieiev.achive;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mykolayevstihnieiev.achive.entities.Descriptors.TaskItemDescriptor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Created by nikolai on 10.11.18.
 */

public class TasksAdapter extends RecyclerView.Adapter<TaskViewHolder> {

    private List<TaskItemDescriptor> taskItemDescriptorList = new ArrayList<TaskItemDescriptor>();

    public void setItems(Collection<TaskItemDescriptor> items) {
        taskItemDescriptorList.addAll(items);
        notifyDataSetChanged();
    }

    public void clearItems() {
        taskItemDescriptorList.clear();
        notifyDataSetChanged();
    }

    public void addTaskItemIntoList() {
        taskItemDescriptorList.add(0, new TaskItemDescriptor(new Random().nextInt(8),"Run marathon"));
        notifyDataSetChanged();
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_item_list, parent, false);
        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        holder.bind(taskItemDescriptorList.get(position));
    }

    @Override
    public int getItemCount() {
        return taskItemDescriptorList.size();
    }
}
