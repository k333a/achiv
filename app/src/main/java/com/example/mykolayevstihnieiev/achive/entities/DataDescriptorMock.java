package com.example.mykolayevstihnieiev.achive.entities;

import com.example.mykolayevstihnieiev.achive.entities.Descriptors.DataDescriptor;

import java.util.ArrayList;
import java.util.Random;

public class DataDescriptorMock extends DataDescriptor {
    public DataDescriptorMock() {
        dataObjects = new ArrayList<>();
        for (int i = 0; i < 300; i++) {
            TaskItemDescriptorMock dt = new TaskItemDescriptorMock();
            dt.categotyImageId = new Random().nextInt(8);
            dt.setId(i);
            dt.title += i;
            dataObjects.add(dt);
        }
    }
}