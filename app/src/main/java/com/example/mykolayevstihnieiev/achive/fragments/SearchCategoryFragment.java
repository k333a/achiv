package com.example.mykolayevstihnieiev.achive.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mykolayevstihnieiev.achive.CategoriesAdapter;
import com.example.mykolayevstihnieiev.achive.entities.Descriptors.CategoryItemDescriptor;
import com.example.mykolayevstihnieiev.achive.R;

import java.util.ArrayList;

public class SearchCategoryFragment extends Fragment {
    private Context context;
    private RecyclerView content;
    private CategoriesAdapter categoriesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.tasks_and_categories_fragment, container, false);
        content = view.findViewById(R.id.myTasksList);
        context = content.getContext();

        content.setLayoutManager(new GridLayoutManager(context, 1));
        categoriesAdapter = new CategoriesAdapter();
        content.setAdapter(categoriesAdapter);

        new Loader().execute();

        return view;
    }

    private class Loader extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            ArrayList<CategoryItemDescriptor> categoryItemDescriptors = new ArrayList<CategoryItemDescriptor>();

            categoryItemDescriptors.add(new CategoryItemDescriptor("Events", 5, 0));
            categoryItemDescriptors.add(new CategoryItemDescriptor("Travel", 1887, 1));
            categoryItemDescriptors.add(new CategoryItemDescriptor("Sport", 342, 2));
            categoryItemDescriptors.add(new CategoryItemDescriptor("Food", 1887, 3));
            categoryItemDescriptors.add(new CategoryItemDescriptor("Culture", 224, 4));
            categoryItemDescriptors.add(new CategoryItemDescriptor("Home and Family", 94, 5));
            categoryItemDescriptors.add(new CategoryItemDescriptor("Education", 114, 6));
            categoryItemDescriptors.add(new CategoryItemDescriptor("Personal", 15654, 7));

            return categoryItemDescriptors;
        }

        @Override
        protected void onPostExecute(Object o) {
            categoriesAdapter.setItems((ArrayList<CategoryItemDescriptor>) o);
        }
    }
}
