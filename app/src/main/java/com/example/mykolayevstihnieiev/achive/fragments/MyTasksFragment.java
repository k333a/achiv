package com.example.mykolayevstihnieiev.achive.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mykolayevstihnieiev.achive.R;
import com.example.mykolayevstihnieiev.achive.TasksAdapter;
import com.example.mykolayevstihnieiev.achive.entities.DataDescriptorMock;
import com.example.mykolayevstihnieiev.achive.entities.DataManager;
import com.example.mykolayevstihnieiev.achive.entities.Descriptors.TaskItemDescriptor;

import java.util.ArrayList;

public class MyTasksFragment extends Fragment {
    private Context context;
    private RecyclerView content;
    private DataManager manager;
    private TasksAdapter categoriesAdapter;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.tasks_and_categories_fragment, container, false);
        context = inflater.getContext();
        content = view.findViewById(R.id.myTasksList);
        content.removeAllViews();

        manager = new DataManager(context);

        content.setLayoutManager(new LinearLayoutManager(context));
        categoriesAdapter = new TasksAdapter();
        content.setAdapter(categoriesAdapter);

        new Loader().execute();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        manager.saveJSONData(new DataDescriptorMock());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        manager.saveJSONData(new DataDescriptorMock());
    }

    public void addNotes() {
        categoriesAdapter.addTaskItemIntoList();
    }

    private class Loader extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            ArrayList<TaskItemDescriptor> taskItemDescriptors = new ArrayList<TaskItemDescriptor>();

            try {
                taskItemDescriptors = manager.getDataDescriptor().dataObjects;
            } catch (Exception e) {

            }

            return taskItemDescriptors;
        }

        @Override
        protected void onPostExecute(Object o) {
            if (o == null) {
                return;
            }

            categoriesAdapter.setItems((ArrayList<TaskItemDescriptor>) o);
        }
    }
}
