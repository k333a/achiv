package com.example.mykolayevstihnieiev.achive;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mykolayevstihnieiev.achive.entities.Descriptors.CategoryItemDescriptor;

/**
 * Created by nikolai on 10.11.18.
 */

public class CategoryViewHolder extends RecyclerView.ViewHolder {
    private ImageView groupCard;
    private TextView title;
    private TextView tasksAmount;

    public CategoryViewHolder(View itemView) {
        super(itemView);

        groupCard = itemView.findViewById(R.id.groupCard);
        title = itemView.findViewById(R.id.group_name);
        tasksAmount = itemView.findViewById(R.id.group_tasks_amount);
    }


    public void bind(CategoryItemDescriptor item) {
        title.setText(item.getTitle());
        tasksAmount.setText(String.valueOf(item.getAmaountOfTasks()) + " avaliable tasks");
        setCategoryImage(item.getCategoryId());
    }

    public void setCategoryImage(int weatherID) {
        switch (weatherID) {
            case 0:
//EVENTS
                groupCard.setBackgroundResource(R.drawable.group_events_background);
                break;
            case 1:
//TRAVEL
                groupCard.setBackgroundResource(R.drawable.group_travel_background);
                break;
            case 2:
//SPORT
                groupCard.setBackgroundResource(R.drawable.group_sport_background);
                break;
            case 3:
//FOOD
                groupCard.setBackgroundResource(R.drawable.group_cooking_background);
                break;
            case 4:
//CULTURE
                groupCard.setBackgroundResource(R.drawable.group_culture_background);
                break;
            case 5:
//HOME AND FAMILY
                groupCard.setBackgroundResource(R.drawable.group_family_background);
                break;
            case 6:
//EDUCATION
                groupCard.setBackgroundResource(R.drawable.group_education_background);
                break;
            case 7:
//PERSONAL
                groupCard.setBackgroundResource(R.drawable.group_personal_background);
                break;
        }
    }
}
