package com.example.mykolayevstihnieiev.achive.entities;

import android.content.Context;

import com.example.mykolayevstihnieiev.achive.entities.Descriptors.DataDescriptor;
import com.example.mykolayevstihnieiev.achive.entities.Descriptors.TaskItemDescriptor;
import com.example.mykolayevstihnieiev.achive.R;
import com.google.gson.Gson;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DataManager {
    private static DataDescriptor dataDescriptor;
    private static Context context;
    private static String filename;

    public DataManager(Context context) {
        this.context = context;
        this.filename = context.getString(R.string.data_file_name);
    }

    public static DataDescriptor getDataDescriptor() {
        if (dataDescriptor == null) {
            loadDataFromJson();
        }
        return dataDescriptor;
    }

    public static TaskItemDescriptor getDataObject(double dataObjectId) {
        for (TaskItemDescriptor obj : getDataDescriptor().dataObjects) {
            if (obj.id == dataObjectId) return obj;
        }
        return null;
    }

    private static void loadDataFromJson() {
        Gson gson = new Gson();
        try {
            File file = new File(context.getFilesDir(), filename);
            dataDescriptor = gson.fromJson(new FileReader(file), DataDescriptor.class);
        } catch (FileNotFoundException e) {
            // NOE - DO NOTHING
        }
    }

    public void saveJSONData(DataDescriptor dataObject) {
        File file = new File(context.getFilesDir(), filename);
        try (BufferedWriter bw = new BufferedWriter(
                new FileWriter(file))) {
            String text = new Gson().toJson(dataObject);
            System.out.println(text);
            bw.write(text);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
