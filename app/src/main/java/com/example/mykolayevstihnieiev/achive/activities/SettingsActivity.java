package com.example.mykolayevstihnieiev.achive.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;

import com.example.mykolayevstihnieiev.achive.R;
import com.example.mykolayevstihnieiev.achive.views.ChooseItemView;
import com.example.mykolayevstihnieiev.achive.views.CustomTextInputView;

public class SettingsActivity extends AppCompatActivity {
    Toolbar toolbar;
    ChooseItemView photos;
    ChooseItemView location;
    ChooseItemView access;

    CustomTextInputView summary;
    CustomTextInputView category;
    CustomTextInputView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Window window = this.getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorAccentDark));

        toolbar = findViewById(R.id.create_task_toolbar);
        toolbar.setTitle(R.string.create_task_activity_title);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        photos = findViewById(R.id.photo_choose);
        location = findViewById(R.id.location_choose);
        access = findViewById(R.id.access_choose);
        summary = findViewById(R.id.summary);
        category = findViewById(R.id.category);
        description = findViewById(R.id.description);

        summary.getEditContentContainer().setHint("Enter summary");
        summary.getTextHint().setText("Enter summary");

        category.getEditContentContainer().setHint("Choose category");
        category.getContent().setCompoundDrawablePadding(4);
        category.getContent().setCompoundDrawables(getDrawable(R.drawable.create_task_choose_category_icon), null, null, null);
        category.getTextHint().setText("Choose category");

        description.getEditContentContainer().setHint("Enter description");
        description.getTextHint().setText("Enter description");

        setPhotosContent();
        setLocationContent();
        setAccessContent();
    }

    private void setPhotosContent() {
        photos.initFields();
        photos.getContainer().setBackground(getResources().getDrawable(R.drawable.create_task_background_photos));
        photos.getText().setText("Photos");
        photos.getImage().setImageResource(R.drawable.common_choose_photos);
    }

    private void setLocationContent() {
        location.initFields();
        location.getContainer().setBackground(getResources().getDrawable(R.drawable.create_task_background_location));
        location.getText().setText("Location");
        location.getImage().setImageResource(R.drawable.common_location_add);
    }

    private void setAccessContent() {
        access.initFields();
        access.getContainer().setBackground(getResources().getDrawable(R.drawable.create_task_background_access));
        access.getText().setText("Access");
        access.getImage().setImageResource(R.drawable.common_group_icon);
    }
}
